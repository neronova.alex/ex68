<?php

namespace Tests\Feature;

use App\Models\Article;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Laravel\Passport\Passport;
use Tests\TestCase;

class CommentTest extends TestCase
{
    use WithFaker;

    /**
     * @var Collection|User
     */
    private $user;

    /**
     * @var Collection/Article[]
     */
    private $articles;

    /**
     * @var Collection/Comment[]
     */
    private $comments;

    /**
     * @var Collection|Article
     */
    private $article;

    /**
     * @var Collection/Comment
     */
    private $comment;

    protected function setUp(): void
    {
        parent::setUp();
        $this->user = \App\Models\User::factory()->create();
        $this->articles = \App\Models\Article::factory()->count(10)->for($this->user)->create();
        $this->article = $this->articles->first();
        $this->comments = \App\Models\Comment::factory()->count(10)->for($this->article)->for($this->user)->create();
    }

    /**
       * Success for user can see comment.
        * @group comments
        * @return void
       */
    public function test_can_get_all_comments()
    {
        $article = $this->articles->random();
        $request = $this->getJson(route('articles.comments.index', ['article' => $article]));
        $request->assertSuccessful();
        $payload = json_decode($request->getContent());
        $this->assertArrayHasKey('data', (array)$payload);
        $this->assertCount($this->comments->count(), $payload->data);

    }

    /**
     * Failed get comment.
     * @group comments
     * @return void
     */
    public function test_get_comment()
    {
        $article = $this->articles->random();
        $comment = $this->comments->random();
        $request = $this->getJson(route('articles.comments.show', ['article' => $article, 'comment' => $comment]));
        $request->assertUnauthorized();

    }

    /**
     * Success get comment.
     * @group comments1
     * @return void
     */
        public function test_success_get_comment()
    {
        $article = $this->articles->random();
        $comment = $this->comments->random();

        Passport::actingAs(
            $this->user,
            [route('articles.comments.show', ['article' => $article, 'comment' => $comment])]
        );
        $request = $this->getJson(route('articles.comments.show', ['article' => $article, 'comment' => $comment]));
        $request->assertSuccessful();
        $payload = json_decode($request->getContent());
        $this->assertArrayHasKey('data', (array)$payload);
        $this->assertArrayHasKey('id', (array)$payload->data);
        $this->assertEquals($comment->id, $payload->data->id);
        $this->assertArrayHasKey('body', (array)$payload->data);
        $this->assertEquals($comment->body, $payload->data->body);
    }


    /**
     * Success create comment.
     * @group comments
     * @return void
     */
    public function test_can_create_comment()
    {
        $article = $this->articles->random();
        Passport::actingAs(
            $this->user,
            [route('articles.comments.store',  ['article' => $article])]
        );
        $user = \App\Models\User::factory()->create();
        $data = [
            'body' => $this->faker->paragraph(3),
            'user_id' => $this->user->id,
            'article_id' => $this->article->id
        ];
        $request = $this->postJson(route('articles.comments.store', ['article' => $article]), $data);
        $payload = json_decode($request->getContent());
        $request->assertCreated();
        $this->assertDatabaseHas('comments', $data);
        $this->assertDatabaseHas('comments', ['id' => $payload->data->id]);
    }

    /**
     * Failed create comment.
     * @group comments
     * @return void
     */
    public function test_body_validation_error()
    {
        $article = $this->articles->random();
        Passport::actingAs(
            $this->user,
            [route('articles.comments.store',  ['article' => $article])]
        );
        $data = [
            'body' => $this->faker->sentences(6),
            'user_id' => $this->user->id,
            'article_id' => $this->article->id
        ];
        $request = $this->postJson(route('articles.comments.store', ['article' => $article]), $data);
        $payload = json_decode($request->getContent());
        $request->assertStatus(422);
        $this->assertArrayHasKey('message', (array)$payload);
        $this->assertArrayHasKey('errors', (array)$payload);
        $this->assertArrayHasKey('body', (array)$payload->errors);
    }

    /**
     * Failed create comment.
     * @group comments
     * @return void
     */
    public function test_validation_error()
    {
        $article = $this->articles->random();
        Passport::actingAs(
            $this->user,
            [route('articles.comments.store',  ['article' => $article])]
        );
        $data = [];
        $request = $this->postJson(route('articles.comments.store', ['article' => $article]), $data);
        $payload = json_decode($request->getContent());
        $request->assertStatus(422);
        $this->assertArrayHasKey('message', (array)$payload);
        $this->assertArrayHasKey('errors', (array)$payload);
        $this->assertArrayHasKey('body', (array)$payload->errors);
        $this->assertArrayHasKey('article_id', (array)$payload->errors);
        $this->assertArrayHasKey('user_id', (array)$payload->errors);
    }


    /**
     * Failed destroy comment.
     * @group comments
     * @return void
     */
    public function test_failed_destroy_comment()
    {
        $article = $this->articles->random();
        $comment = $this->comments->random();
        $response = $this->json('delete', route('articles.comments.destroy', ['article' => $article, 'comment' => $comment]));
        $response->assertUnauthorized();;
    }

}
