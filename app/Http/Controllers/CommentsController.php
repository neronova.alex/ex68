<?php

namespace App\Http\Controllers;

use App\Http\Requests\CommentRequest;
use App\Http\Resources\CommentResource;
use App\Models\Article;
use App\Models\Comment;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Response;

class CommentsController extends Controller
{
    /**
     * CommentsController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth:api')->except(['index']);
    }

    /**
     * @return AnonymousResourceCollection
     */
    public function index(): AnonymousResourceCollection
    {
        $comments = Comment::all();
        return CommentResource::collection($comments);
    }


    /**
     * @param CommentRequest $request
     * @param Comment $comment
     * @return CommentResource
     */
    public function store(CommentRequest $request, Comment $comment): CommentResource
    {
        $comment = Comment::create($request->all());
        return new CommentResource($comment);
    }


    /**
     * @param Comment $comment
     * @return CommentResource
     */
    public function show(Comment $comment): CommentResource
    {
        return new CommentResource($comment);
    }


    /**
     * @param Request $request
     * @param Comment $comment
     * @return CommentResource
     */
    public function update(Request $request, Comment $comment): CommentResource
    {
        $comment->update($request->all());
        return new CommentResource($comment);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Comment $comment): Response
    {
        $comment->delete();
        return response('', 204);
    }
}
